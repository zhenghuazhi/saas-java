package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public enum FunProcess {

	// 新建
	NEW("10","bus.funprocess.new"),

	// 变更
	CHANGE("20","bus.funprocess.change"),

	// 注销
	DESTORY("40","bus.funprocess.destory");

	private final String val;
	private final String label;

	private static Map<String, FunProcess> busStateMap;

	FunProcess(String val, String label) {
		this.val = val;
		this.label = label;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return label;
	}

	public static FunProcess getInstByVal(String key) {
		synchronized (FunProcess.class) {
			if (busStateMap == null) {
				busStateMap = new HashMap<String, FunProcess>();
				for (FunProcess busState : FunProcess.values()) {
					busStateMap.put(busState.getVal(), busState);
				}
			}
		}
		if (!busStateMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的FunProcess枚举值不存在。");
		}
		return busStateMap.get(key);
	}

}
