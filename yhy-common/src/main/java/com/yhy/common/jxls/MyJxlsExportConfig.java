package com.yhy.common.jxls;

/**
 * <br>
 * <b>功能：</b><br>
 * <b>作者：</b>yanghuiyuan<br>
 * <b>日期：</b> 2020/3/23 <br>
 * <b>版权所有：<b>版权所有(C) 2020<br>
 */
public class MyJxlsExportConfig {
    // 模板路径
    private String templatePath;
    // 导出类
    private String exportSupportBean;

    public MyJxlsExportConfig() {
    }

    public MyJxlsExportConfig(String templatePath, String exportSupportBean) {
        this.templatePath = templatePath;
        this.exportSupportBean = exportSupportBean;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getExportSupportBean() {
        return exportSupportBean;
    }

    public void setExportSupportBean(String exportSupportBean) {
        this.exportSupportBean = exportSupportBean;
    }
}
