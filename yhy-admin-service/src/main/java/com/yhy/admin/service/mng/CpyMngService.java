package com.yhy.admin.service.mng;

import com.yhy.admin.dao.CpyMngDao;
import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.service.CpyService;
import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.admin.vo.CpyVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.constants.OperateType;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseMngService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:24 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午10:18 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CpyMngService extends BaseMngService<CpyDTO,CpyMngVO> {

    @Autowired
    private CpyMngDao cpyMngDao;

    @Autowired
    private CpyService cpyService;

    @Autowired
    private OrgSetMngService orgSetMngService;

    @Autowired
    private SysRoleMngService sysRoleMngService;


    @Override
    public CpyService getBaseMainService() {
        return cpyService;
    }

    @Override
    protected CpyMngDao getBaseMngDao() {
        return cpyMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_CPY_MNG.getVal();
    }

    @Override
    protected void beforeSaveOfProcess(CpyDTO baseDTO) {
        super.beforeSaveOfProcess(baseDTO);
        CpyMngVO cpyMngVO = baseDTO.getBusMainData();
        CpyVO paramVO = new CpyVO();
        paramVO.setEnableFlag("Y");
        paramVO.setCerCode(cpyMngVO.getCerCode());
        List<CpyVO> cpyVOList = getBaseMainService().findBy(paramVO);
        for (CpyVO tmpVO : cpyVOList) {
            if(!tmpVO.getId().equals(cpyMngVO.getId())) {
                throw new BusinessException("相同的证件编号已存在");
            }
        }
    }

    @Override
    protected void processOfToAdd(CpyDTO baseDTO) {
        super.processOfToAdd(baseDTO);
        baseDTO.getBusMainData().setEnableFlag("Y");
        baseDTO.getBusMainData().setCpyStatus("ONLINE");
    }

    @Override
    protected void processOfToCopy(CpyDTO baseDTO) {
        super.processOfToCopy(baseDTO);
        CpyMngVO busMainData = baseDTO.getBusMainData();
        busMainData.setCpyCode(null);
    }

    @Override
    protected void afterProcessCustomDataOfSave(CpyDTO baseDTO) {
        super.afterProcessCustomDataOfSave(baseDTO);
        if(OperateType.CREATE.getVal().equals(baseDTO.getOperateType())) {
            CpyMngVO cpyMngVO = baseDTO.getBusMainData();
            CpyVO cpyVO = getBaseMainService().findById(cpyMngVO.getId());
            //自动建立一个默认机构
            String orgsetId = orgSetMngService.autoCreateDefaultOrgSet(cpyVO.getCpyCode());

            sysRoleMngService.autoCreateDefaultRole(cpyVO.getCpyCode(),orgsetId);
        }
    }

}
