package com.yhy.admin.service;

import com.yhy.admin.dao.SysRoleMenuDao;
import com.yhy.admin.dao.SysRoleOrgDao;
import com.yhy.admin.vo.SysRoleMenuVO;
import com.yhy.admin.vo.SysRoleOrgVO;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-8 下午6:24 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleOrgService extends BaseService<SysRoleOrgVO> {


    @Autowired
    private SysRoleOrgDao baseDao;

    @Override
    protected SysRoleOrgDao getDao() {
        return baseDao;
    }

    public int deleteByRoleId(String roleId) {
        return getDao().deleteByRoleId(roleId);
    }

    public List<SysRoleOrgVO> findByRoleId(String roleId) {
        return getDao().findByRoleId(roleId);
    }

}
