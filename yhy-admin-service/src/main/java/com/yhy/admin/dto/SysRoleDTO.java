package com.yhy.admin.dto;

import com.yhy.admin.vo.*;
import com.yhy.admin.vo.mng.SysRoleMngVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.dto.BaseMngDTO;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-9 下午3:17 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public class SysRoleDTO extends BaseMngDTO<SysRoleMngVO> {

    private SysUserMngVO userParamBean;

    @Override
    public SysRoleVO getBusMain() {
        return new SysRoleVO();
    }

    public SysUserMngVO getUserParamBean() {
        return userParamBean;
    }

    public void setUserParamBean(SysUserMngVO userParamBean) {
        this.userParamBean = userParamBean;
    }
}
