package com.yhy.job.utils;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午3:27 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public abstract class BaseJob  implements Runnable {

    public abstract void exec(String methodParam);

    private String methodParam;

    @Override
    public void run() {
        exec(methodParam);
    }

    public String getMethodParam() {
        return methodParam;
    }

    public void setMethodParam(String methodParam) {
        this.methodParam = methodParam;
    }
}
