package com.yhy.common.exception;

/**
 * <br>
 * <b>功能：</b>运行时异常基类<br>
 * <b>作者：</b>yanghuiyuan<br>
 * <b>日期：</b> 2019-9-10 <br>
 */
public class CustomRuntimeException extends RuntimeException {
	
	public CustomRuntimeException(String message) {
		super(message);
	}

	public CustomRuntimeException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
