package com.yhy;

import com.yhy.common.config.EnvConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableDiscoveryClient
//@EnableFeignClients
@EnableTransactionManagement
@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
})
public class WebMainApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(WebMainApp.class,args);
        //加载初始参数
        context.getBean(EnvConfig.class).set();
    }

}
