package com.yhy.admin.vo;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-12-26 下午5:15 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class UserMgrVO extends BaseEntity {

    private String userId;
    private String userAccount;
    private String cpyCode;
    private String mgrAccount;

    public UserMgrVO() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }

    public String getMgrAccount() {
        return mgrAccount;
    }

    public void setMgrAccount(String mgrAccount) {
        this.mgrAccount = mgrAccount;
    }
}
