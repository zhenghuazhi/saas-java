package com.yhy.admin.service;

import com.yhy.admin.dao.OrgSetDao;
import com.yhy.admin.vo.OrgSetVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.utils.YhyUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class OrgSetService extends BaseMainService<OrgSetVO> {

    @Autowired
    private OrgSetDao orgSetDao;

    @Override
    protected OrgSetDao getDao() {
        return orgSetDao;
    }


    @Override
    protected void preInsert(OrgSetVO entity) {
        super.preInsert(entity);
        if(StringUtils.isBlank(entity.getOrgCode())) {
            entity.setOrgCode(YhyUtils.genSysCode(BusModuleType.MD_ORG_SET_MNG.getVal()));
        }
    }

}
