package com.yhy.admin.vo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:42 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class SysUserJobVO extends BaseEntity {
	
    /**
     * 用户ID db_column: USER_ID 
     */	
	private String userId;
    /**
     * 职务名称 db_column: JOB_NAME 
     */	
	private String jobName;
    /**
     * 职位ID db_column: JOB_ID 
     */	
	private String jobId;
    /**
     * 公司编号 db_column: CPY_CODE 
     */	
	private String cpyCode;

	public SysUserJobVO(){
	}

	public SysUserJobVO( String id ){
		this.id = id;
	}

	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return this.userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getJobName() {
		return this.jobName;
	}
	
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobId() {
		return this.jobId;
	}
	
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getCpyCode() {
		return this.cpyCode;
	}
	
	public void setCpyCode(String cpyCode) {
		this.cpyCode = cpyCode;
	}

}

