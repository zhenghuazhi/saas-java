package com.yhy.common.service;

import com.aspose.cells.*;
import com.yhy.common.exception.BusinessException;

import java.io.*;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-31 上午9:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class BusAsposeExcelService {

    /**
     * 获取license
     *
     * @return
     */
    private static boolean getLicense() {
        boolean result = false;
        try {
            // 凭证
            String license =
                    "<License>\n" +
                    "  <Data>\n" +
                    "    <Products>\n" +
                    "      <Product>Aspose.Total for Java</Product>\n" +
                    "      <Product>Aspose.Words for Java</Product>\n" +
                    "    </Products>\n" +
                    "    <EditionType>Enterprise</EditionType>\n" +
                    "    <SubscriptionExpiry>20991231</SubscriptionExpiry>\n" +
                    "    <LicenseExpiry>20991231</LicenseExpiry>\n" +
                    "    <SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber>\n" +
                    "  </Data>\n" +
                    "  <Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature>\n" +
                    "</License>";
            InputStream is = new ByteArrayInputStream(license.getBytes("UTF-8"));
            License asposeLic = new License();
            asposeLic.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void excel2html(String excelPath,String htmlPath) {
        if (!getLicense()) {
            return;
        }
        FileOutputStream fileOS = null;
        try {
            File pdfFile = new File(htmlPath);// 输出路径

            HtmlSaveOptions saveOptions = new HtmlSaveOptions();
            saveOptions.setEncoding(Encoding.getEncoding("UTF-8"));
            saveOptions.setExportHiddenWorksheet(true);
            saveOptions.setCreateDirectory(true);
            saveOptions.setExportActiveWorksheetOnly(true);
            saveOptions.setSortNames(true);
            saveOptions.setExportImagesAsBase64(true);
            fileOS = new FileOutputStream(pdfFile);
            Workbook convertExcel = new Workbook(new FileInputStream(excelPath));
            convertExcel.save(fileOS,saveOptions);
            fileOS.close();
        }catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("转换HTML出错"+e.getMessage());
        } finally {
            if(fileOS != null) {
                try {
                    fileOS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * excel 转 pdf
     * @param excelPath 要转换的excel文件路径
     * @param pdfPath   转换完成后输出的pdf文件路径
     */
    public static void excel2pdf(String excelPath,String pdfPath) {
        if (!getLicense()) {
            return;
        }

        try {
            PdfSaveOptions pdfSaveOptions = new PdfSaveOptions();
            pdfSaveOptions.setOnePagePerSheet(true);
            Workbook convertExcel = new Workbook(new FileInputStream(excelPath));
            convertExcel.save(pdfPath, SaveFormat.PDF);
        }catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("转换PDF出错"+e.getMessage());
        } finally {
        }
    }

    public static void main(String[] args) {
        BusAsposeExcelService.excel2pdf("C:\\Users\\weixb\\Desktop\\wm\\object_collection_template.xlsx",
                "C:\\Users\\weixb\\Desktop\\wm\\test_export.pdf");
    }

}
