package com.yhy.admin.action;

import com.yhy.admin.dto.CpyDTO;
import com.yhy.admin.service.mng.CpyMngService;
import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.constants.CpyStatus;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.intercept.LoginPermission;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午10:20 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/cpyMng", produces="application/json;charset=UTF-8")
public class CpyMngAction extends BaseMngAction<CpyDTO> {

    @Autowired
    private CpyMngService cpyMngService;

    @Override
    protected CpyMngService getBaseService() {
        return cpyMngService;
    }


    /*
     * 获取公司
     */
    @LoginPermission(loginRequired = false)
    @RequestMapping(value="/getAllCpy",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取公司", notes="获取公司")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyName", value = "公司名称", required = true, dataType = "String")
    })
    AppReturnMsg getAllCpy(@RequestParam(name = "cpyName",required = false) String cpyName, HttpServletRequest request, HttpServletResponse response) {
        CpyDTO cpyDTO =new CpyDTO();

        CpyMngVO paramBean = new CpyMngVO();
        paramBean.setCpyStatus(CpyStatus.ONLINE.getVal());
        paramBean.setEnableFlag("Y");
        paramBean.setCpyName(cpyName);
        cpyDTO.setParamBean(paramBean);

        List<CpyMngVO> userMngVOs = cpyMngService.queryPageData(cpyDTO);

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",userMngVOs,null);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = CpyDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody CpyDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = CpyDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody CpyDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody CpyDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody CpyDTO baseDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        sysUser.setCpyCode(null);
        baseDTO.setSysUser(sysUser);
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除数组", required = true, dataType = "BaseDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody CpyDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }
}
