package com.yhy.admin.dto;

import com.yhy.admin.vo.SysCustomFieldVO;
import com.yhy.common.dto.BaseDTO;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-24 上午9:14 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

public class SysCustomFieldDTO extends BaseDTO<SysCustomFieldVO> {

    private List<SysCustomFieldVO> customFieldList;

    public List<SysCustomFieldVO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<SysCustomFieldVO> customFieldList) {
        this.customFieldList = customFieldList;
    }
}
