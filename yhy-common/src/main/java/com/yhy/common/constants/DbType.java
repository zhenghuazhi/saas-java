package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:17 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public enum DbType {

	/**
	 * oracle
	 */
	ORACLE("oracle"),

	MYSQL("mysql");

	private final String val;

	private static Map<String, DbType> dbTypeMap;

	DbType(String val) {
		this.val = val;
	}

	public String getVal() {
		return val;
	}

	public static DbType getInstByVal(String key) {
		if (dbTypeMap == null) {

			synchronized (DbType.class) {
				if (dbTypeMap == null) {
					dbTypeMap = new HashMap<String, DbType>();
					for (DbType busState : DbType.values()) {
						dbTypeMap.put(busState.getVal(), busState);
					}
				}
			}
		}
		if (!dbTypeMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的db枚举值不存在。");
		}
		return dbTypeMap.get(key);
	}

}
