package com.yhy.admin.dao;

import com.yhy.admin.vo.SysCustomFieldVO;

import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:20 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysCustomFieldDao")
public interface SysCustomFieldDao  extends BaseDao<SysCustomFieldVO> {

}
