package com.yhy.admin.vo;

import com.yhy.common.dto.BaseEntity;

/**
 */
public class MenuMetaVo extends BaseEntity {

    private String title;

    private String icon;
    private Boolean keepAlive = true;
    public MenuMetaVo() {
    }

    public MenuMetaVo(String title, String icon) {
        this.title = title;
        this.icon = icon;
    }

    public Boolean getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(Boolean keepAlive) {
        this.keepAlive = keepAlive;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
