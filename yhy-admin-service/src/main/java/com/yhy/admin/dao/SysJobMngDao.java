package com.yhy.admin.dao;

import com.yhy.admin.dto.SysJobDTO;
import com.yhy.admin.vo.mng.SysJobMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午2:06 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Mapper
@Component(value = "sysJobMngDao")
public interface SysJobMngDao extends BaseMngDao<SysJobDTO,SysJobMngVO> {


}
