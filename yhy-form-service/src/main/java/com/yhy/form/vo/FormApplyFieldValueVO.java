package com.yhy.form.vo;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-17 上午10:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FormApplyFieldValueVO extends BaseEntity {
    /**
     * 主表ID db_column: MAIN_ID 
     */	
	private String mainId;
    /**
     * 控件ID db_column: FIELD_CONTROL_ID 
     */	
	private String fieldControlId;
    /**
     * 控件名称 db_column: FIELD_NAME 
     */	
	private String fieldName;
    /**
     * 控件值 db_column: FIELD_VALUE 
     */	
	private String fieldValue;
	private String fieldValueLabel;


	public String getFieldValueLabel() {
		return fieldValueLabel;
	}

	public void setFieldValueLabel(String fieldValueLabel) {
		this.fieldValueLabel = fieldValueLabel;
	}

	public FormApplyFieldValueVO(){
	}

	public FormApplyFieldValueVO( String id ){
		this.id = id;
	}

	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getMainId() {
		return this.mainId;
	}
	
	public void setMainId(String mainId) {
		this.mainId = mainId;
	}
	public String getFieldControlId() {
		return this.fieldControlId;
	}
	
	public void setFieldControlId(String fieldControlId) {
		this.fieldControlId = fieldControlId;
	}
	public String getFieldName() {
		return this.fieldName;
	}
	
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldValue() {
		return this.fieldValue;
	}
	
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

}

