package com.yhy.form.service;

import com.yhy.common.service.BaseMainService;
import com.yhy.form.dao.FormSetMainDao;
import com.yhy.form.vo.FormSetMainVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *  *
 *  *  * <br>
 *  *  * <b>功能：</b> <br>
 *  *  * <b>作者：</b>yanghuiyaun <br>
 *  *  * <b>日期：</b> 20-1-10 上午11:44 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020 <br>
 *  *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FormSetMainService extends BaseMainService<FormSetMainVO> {

    @Autowired
    private FormSetMainDao baseDao;

    @Override
    protected FormSetMainDao getDao() {
        return baseDao;
    }

    public Integer updateFormWidth(String id, Integer formWidth) {
        return baseDao.updateFormWidth(id, formWidth);
    }

}
