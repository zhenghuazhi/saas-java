package com.yhy.job.tasks;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午3:36 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

import com.yhy.job.utils.BaseJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * <br>
 * <b>功能：</b><br>
 * <b>作者：</b>yanghuiyaun<br>
 * <b>日期：</b> 2019/8/16 <br>
 * <b>版权所有：<b>版权所有(C) 2019<br>
 */
@Component
public class TestJobTask extends BaseJob {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void exec(String methodParam) {
        logger.info("任务测试开始"+methodParam);
        try {
            Thread.sleep(1000 * 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("任务测试结束");
    }

}
