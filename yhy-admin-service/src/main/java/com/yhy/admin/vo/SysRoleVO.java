package com.yhy.admin.vo;

import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMainEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-8 下午6:13 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class SysRoleVO extends BaseMainEntity {

    private String roleName;


    public SysRoleVO() {
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
