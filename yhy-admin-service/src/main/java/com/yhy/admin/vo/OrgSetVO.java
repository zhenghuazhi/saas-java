package com.yhy.admin.vo;

import com.yhy.common.dto.BaseMainEntity;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

public class OrgSetVO extends BaseMainEntity {

    private String orgName;
    private String orgType;
    private String parentId;
    private String orgCode;
    private String cpyCode;
    private String orgStatus;
    private String thirdCode;
    private String mgrAccount;


    public OrgSetVO() {
    }

    public String getMgrAccount() {
        return mgrAccount;
    }

    public void setMgrAccount(String mgrAccount) {
        this.mgrAccount = mgrAccount;
    }

    public String getOrgStatus() {
        return orgStatus;
    }

    public void setOrgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getThirdCode() {
        return thirdCode;
    }

    public void setThirdCode(String thirdCode) {
        this.thirdCode = thirdCode;
    }
}
